from rest_framework import viewsets
from rest_framework.response import Response

from .models import Page
from .serializers import PageSerializer, PageSerializerEdit, PageSerializerList
from .tasks import increment_counters


class PageViewSet(viewsets.ModelViewSet):
    queryset = Page.objects.all()

    def get_serializer_class(self):
        if self.action == "list":
            return PageSerializerList
        if self.action in ["create", "update", "partial_update"]:
            return PageSerializerEdit
        return PageSerializer

    def retrieve(self, request, *args, **kwargs):
        instance: Page = self.get_object()
        serializer = self.get_serializer(instance)
        content_items = instance.content_items.values_list("id", flat=True)
        if content_items:
            increment_counters.s([int(i) for i in content_items]).apply_async()
        return Response(serializer.data)
