from django.db import models
from polymorphic.models import PolymorphicModel


class BaseContentModel(models.Model):
    title = models.TextField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        abstract = True


class Page(BaseContentModel):
    title = models.TextField(max_length=500)
    content_items = models.ManyToManyField(
        "content.Content", related_name="pages", blank=True
    )

    class Meta:
        ordering = ["-created_at"]


class Content(PolymorphicModel, BaseContentModel):
    counter = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ["-created_at"]


class Video(Content):
    source = models.URLField()
    subtitles = models.URLField(null=True, blank=True)


class Audio(Content):
    source = models.URLField()
    bitrate = models.PositiveSmallIntegerField()


class TextBlock(Content):
    text = models.TextField()
