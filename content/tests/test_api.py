import pytest

from content.models import Audio, Page, TextBlock, Video


@pytest.mark.django_db
def test_get_pages(drf_client, pages):
    pages = []
    for i in range(6):
        page = Page.objects.create(title=f"Page {i}")
        page.save()
        pages.append(page)

    res = drf_client.get("/page/", format="json")
    assert res.data["count"] == 6
    assert res.data["results"] == [
        {"url": f"http://testserver/page/{p.id}/"} for p in reversed(pages[1:])
    ]


@pytest.mark.django_db
def test_page_detail(drf_client):
    page = Page.objects.create(title=f"Page")
    audio = Audio.objects.create(
        title="Audio", source="http://asdasd.mp3", bitrate=128,
    )
    video = Video.objects.create(
        title="Video", source="http://asdasd.mp4", subtitles="http://asdasd.aac"
    )
    text_block = TextBlock.objects.create(title="Text", text="some text")
    page.content_items.add(audio)
    page.content_items.add(video)
    res = drf_client.get(f"/page/{page.id}/", format="json")
    assert res.data == {
        "title": page.title,
        "content_items": [
            {
                "title": "Video",
                "source": "http://asdasd.mp4",
                "subtitles": "http://asdasd.aac",
                "counter": 1,
                "resourcetype": "Video",
            },
            {
                "title": "Audio",
                "bitrate": 128,
                "source": "http://asdasd.mp3",
                "counter": 1,
                "resourcetype": "Audio",
            },
        ],
    }

    page.content_items.add(text_block)

    res = drf_client.get(f"/page/{page.id}/", format="json")
    assert res.data == {
        "title": page.title,
        "content_items": [
            {
                "title": "Text",
                "text": "some text",
                "counter": 1,
                "resourcetype": "TextBlock",
            },
            {
                "title": "Video",
                "source": "http://asdasd.mp4",
                "subtitles": "http://asdasd.aac",
                "counter": 2,
                "resourcetype": "Video",
            },
            {
                "title": "Audio",
                "bitrate": 128,
                "source": "http://asdasd.mp3",
                "counter": 2,
                "resourcetype": "Audio",
            },
        ],
    }
