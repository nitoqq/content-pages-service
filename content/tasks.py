import typing as t

from django.db.models import F

from mysite.celery import app
from .models import Content


@app.task(bind=True)
def increment_counters(self, id_list: t.List[int]):
    Content.objects.filter(id__in=id_list).update(counter=F("counter") + 1)
