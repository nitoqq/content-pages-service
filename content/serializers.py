from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField
from rest_polymorphic.serializers import PolymorphicSerializer

from .models import Audio, Content, Page, TextBlock, Video


class VideoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Video
        fields = ["title", "source", "subtitles", "counter"]
        read_only_fields = ["counter"]


class AudioSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Audio
        fields = ["title", "bitrate", "source", "counter"]
        read_only_fields = ["counter"]


class TextBlockSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TextBlock
        fields = ["title", "text", "counter"]
        read_only_fields = ["counter"]


class ContentPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        Video: VideoSerializer,
        Audio: AudioSerializer,
        TextBlock: TextBlockSerializer,
    }


class PageSerializer(serializers.HyperlinkedModelSerializer):
    content_items = ContentPolymorphicSerializer(many=True)

    class Meta:
        model = Page

        fields = ["title", "content_items"]


class PageSerializerEdit(PageSerializer):
    content_items = PrimaryKeyRelatedField(many=True, queryset=Content.objects.all())

    class Meta(PageSerializer.Meta):
        pass


class PageSerializerList(PageSerializer):
    class Meta(PageSerializer.Meta):
        extra_kwargs = {"url": {"view_name": "page-detail"}}
        fields = ["url"]
