from django.contrib import admin
from polymorphic.admin import (
    PolymorphicChildModelAdmin,
    PolymorphicChildModelFilter,
    PolymorphicParentModelAdmin,
)

from .models import Audio, Content, Page, TextBlock, Video


@admin.register(Video)
class VideoAdmin(PolymorphicChildModelAdmin):
    fields = ["title", "source", "subtitles", "counter"]
    readonly_fields = ["counter"]
    base_model = Content


@admin.register(Audio)
class AudioAdmin(PolymorphicChildModelAdmin):
    fields = ["title", "source", "bitrate", "counter"]
    readonly_fields = ["counter"]
    base_model = Content


@admin.register(TextBlock)
class TextBlockAdmin(PolymorphicChildModelAdmin):
    fields = ["title", "text", "counter"]
    readonly_fields = ["counter"]
    base_model = Content


@admin.register(Content)
class ModelAParentAdmin(PolymorphicParentModelAdmin):
    base_model = Content
    child_models = (Video, Audio, TextBlock)
    list_filter = (PolymorphicChildModelFilter,)
    search_fields = ["title__startswith"]
    list_display = ("title", "polymorphic_ctype", "counter", "created_at")


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    fields = ["title", "content_items"]
    autocomplete_fields = ["content_items"]
    search_fields = ["title__startswith"]
    list_display = ("title", "created_at")
