FROM python:3.7-slim-buster

ENV PIP_DEFAULT_TIMEOUT=100
ENV PIP_NO_CACHE_DIR=off
ENV PIP_DISABLE_PIP_VERSION_CHECK=on
ENV PYTHONDONTWRITEBYTECODE=true

# hadolint ignore=DL3013
RUN DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && rm -rf /var/lib/apt/lists/*

RUN pip install pip==19.2.3

WORKDIR /app/

COPY /requirements/  ./requirements/

RUN pip install -r ./requirements/prod.txt

COPY ./ /app/

CMD ["./runserver.sh"]
