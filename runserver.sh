#!/usr/bin/env sh

python manage.py collectstatic --no-input
python manage.py migrate
python manage.py loaddata fixtures.json
gunicorn --workers=4 mysite.wsgi -b 0.0.0.0:8080
