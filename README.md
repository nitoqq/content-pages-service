Сервис контентных страниц
-------------------------

Запуск
```bash
dokcer-compose up runserver
```

Запуск тестов

```bash
dokcer-compose up runtests
```

Примеры запросов:

Получение списка страниц
```bash
curl http://0.0.0.0:8000/page/?format=json
```
Ответ в формате JSON
```json
{
  "count":6,
  "next":"http://0.0.0.0:8000/page/?format=json&page=2",
  "previous":null,
  "results":[
    {"url":"http://0.0.0.0:8000/page/7/?format=json"},
    {"url":"http://0.0.0.0:8000/page/6/?format=json"},
    {"url":"http://0.0.0.0:8000/page/5/?format=json"},
    {"url":"http://0.0.0.0:8000/page/4/?format=json"},
    {"url":"http://0.0.0.0:8000/page/3/?format=json"}
]}
```

Детальная информация о странице
```bash
http://0.0.0.0:8000/page/7/?format=json
```
Ответ в формате JSON
```json
{
  "title":"Page 6",
  "content_items":[
    {
      "title":"Video 1",
      "source":"https://example.com/video.mp4",
      "subtitles":null,
      "counter":20,
      "resourcetype":"Video"
    }, {
      "title":"Text block",
      "text":"Text block asdasda asdasd",
      "counter":21,
      "resourcetype":"TextBlock"
    }, {
      "title":"Some audio",
      "bitrate":128,
      "source":"https://example.com/audio.mp3",
      "counter":9,
      "resourcetype":"Audio"
    },{
      "title": "Some video",
      "source": "https://example.com/video.mp4",
      "subtitles": "https://example.com/video.acc",
      "counter": 8,
      "resourcetype": "Video"
    }]
}
```

Так же по адресу http://0.0.0.0:8000/ доступен WEB-client

По адресу http://0.0.0.0:8000/admin доступен интрефейс администратора пользователь: admin пароль: kek
